# Quickstart Chrome Extension

Quick start project template for building a Chrome extension

Includes Browser Sync and file watching with automatic asset compile & copy.

## Usage

Install, then start Browser Sync and watch for file changes:

    npm i
    npm run start

## Tasks

### Build

Compile assets and copy them to the extension directory.

- `build`
- `build:scripts`
- `build:styles`

### Watch

Watch src assets. Recompiles them when edited, then copies them to the extension directory.

`npm run watch`

### Vendor

Vendor assets are not included in the watch task. Copy them after edits with either:

- `build`
- `copy`
- `copy:vendor`

### More Tasks

Compile assets:

- `compile` (includes all below)
- `compile:scripts`
- `compile:scripts:content`
- `compile:scripts:options`
- `compile:scripts:popup`
- `compile:styles`

Copy assets to `./extension` with:

- `copy` (includes all below)
- `copy:scripts`
- `copy:styles`
- `copy:html`
- `copy:images`
- `copy:vendor`
